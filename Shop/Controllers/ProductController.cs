﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using Shop.Models.RESTModels;
using Shop.Services;

namespace Shop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;
        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProductRestItem>> GetAllProducts()
        {

            return Ok(productService.GetProducts());
        }

        [HttpGet("{id}")]
        public ActionResult<ProductRestItem> GetProduct(int id)
        {
            var result = productService.GetProduct(id);
            if (result == null)
            {
                return BadRequest("Product with such id is not found");
            }
            return Ok(result);
        }
        [HttpPost]
        public ActionResult<string> CreateProduct([FromBody]ProductRestItem product)
        {

            return Ok(productService.AddProduct(product));
        }
    }
}