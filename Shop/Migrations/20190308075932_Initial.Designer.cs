﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Shop.Models.DBModels;

namespace Shop.Migrations
{
    [DbContext(typeof(ShopAplicationContext))]
    [Migration("20190308075932_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.1-servicing-10028")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Shop.Models.DBModels.GroupDBItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.Property<int?>("ParentId");

                    b.HasKey("Id");

                    b.ToTable("Groups");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Main"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Sub1",
                            ParentId = 1
                        },
                        new
                        {
                            Id = 3,
                            Name = "Subsub1",
                            ParentId = 2
                        },
                        new
                        {
                            Id = 4,
                            Name = "Sub2",
                            ParentId = 1
                        },
                        new
                        {
                            Id = 5,
                            Name = "SubSub2",
                            ParentId = 4
                        },
                        new
                        {
                            Id = 6,
                            Name = "Subsub3",
                            ParentId = 2
                        },
                        new
                        {
                            Id = 7,
                            Name = "Subsub5",
                            ParentId = 4
                        });
                });

            modelBuilder.Entity("Shop.Models.DBModels.ProductToGroups", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GroupId");

                    b.Property<int>("ProductId");

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductToGroups");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            GroupId = 1,
                            ProductId = 1
                        },
                        new
                        {
                            Id = 2,
                            GroupId = 2,
                            ProductId = 1
                        },
                        new
                        {
                            Id = 3,
                            GroupId = 3,
                            ProductId = 1
                        },
                        new
                        {
                            Id = 4,
                            GroupId = 4,
                            ProductId = 1
                        },
                        new
                        {
                            Id = 5,
                            GroupId = 5,
                            ProductId = 1
                        },
                        new
                        {
                            Id = 6,
                            GroupId = 1,
                            ProductId = 2
                        },
                        new
                        {
                            Id = 7,
                            GroupId = 2,
                            ProductId = 2
                        },
                        new
                        {
                            Id = 8,
                            GroupId = 3,
                            ProductId = 2
                        });
                });

            modelBuilder.Entity("Shop.Models.DBModels.ProductToShop", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ProductId");

                    b.Property<int>("ShopId");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.HasIndex("ShopId");

                    b.ToTable("ProductToShop");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ProductId = 1,
                            ShopId = 1
                        },
                        new
                        {
                            Id = 2,
                            ProductId = 1,
                            ShopId = 2
                        },
                        new
                        {
                            Id = 3,
                            ProductId = 1,
                            ShopId = 3
                        },
                        new
                        {
                            Id = 4,
                            ProductId = 2,
                            ShopId = 1
                        },
                        new
                        {
                            Id = 5,
                            ProductId = 2,
                            ShopId = 3
                        });
                });

            modelBuilder.Entity("Shop.Models.ProductDbItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("AddedTime");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.Property<double>("PriceVat");

                    b.Property<double>("VatRate");

                    b.HasKey("Id");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AddedTime = new DateTime(2019, 3, 8, 9, 59, 32, 428, DateTimeKind.Local).AddTicks(5834),
                            Name = "Apple",
                            Price = 300.0,
                            PriceVat = 450.0,
                            VatRate = 0.5
                        },
                        new
                        {
                            Id = 2,
                            AddedTime = new DateTime(2019, 3, 8, 9, 59, 32, 430, DateTimeKind.Local).AddTicks(8021),
                            Name = "Orange",
                            Price = 200.0,
                            PriceVat = 400.0,
                            VatRate = 2.0
                        });
                });

            modelBuilder.Entity("Shop.Models.ShopDbItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Shops");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Shop#1"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Shop#2"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Shop#3"
                        });
                });

            modelBuilder.Entity("Shop.Models.DBModels.ProductToGroups", b =>
                {
                    b.HasOne("Shop.Models.DBModels.GroupDBItem", "Group")
                        .WithMany()
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Shop.Models.ProductDbItem", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Shop.Models.DBModels.ProductToShop", b =>
                {
                    b.HasOne("Shop.Models.ProductDbItem", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Shop.Models.ShopDbItem", "Shop")
                        .WithMany()
                        .HasForeignKey("ShopId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
