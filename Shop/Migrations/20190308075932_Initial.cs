﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    PriceVat = table.Column<double>(nullable: false),
                    AddedTime = table.Column<DateTime>(nullable: false),
                    VatRate = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shops",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shops", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductToGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    GroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductToGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductToGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductToGroups_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductToShop",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    ShopId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductToShop", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductToShop_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductToShop_Shops_ShopId",
                        column: x => x.ShopId,
                        principalTable: "Shops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Name", "ParentId" },
                values: new object[,]
                {
                    { 1, "Main", null },
                    { 2, "Sub1", 1 },
                    { 3, "Subsub1", 2 },
                    { 4, "Sub2", 1 },
                    { 5, "SubSub2", 4 },
                    { 6, "Subsub3", 2 },
                    { 7, "Subsub5", 4 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "AddedTime", "Name", "Price", "PriceVat", "VatRate" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 3, 8, 9, 59, 32, 428, DateTimeKind.Local).AddTicks(5834), "Apple", 300.0, 450.0, 0.5 },
                    { 2, new DateTime(2019, 3, 8, 9, 59, 32, 430, DateTimeKind.Local).AddTicks(8021), "Orange", 200.0, 400.0, 2.0 }
                });

            migrationBuilder.InsertData(
                table: "Shops",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Shop#1" },
                    { 2, "Shop#2" },
                    { 3, "Shop#3" }
                });

            migrationBuilder.InsertData(
                table: "ProductToGroups",
                columns: new[] { "Id", "GroupId", "ProductId" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 2, 2, 1 },
                    { 3, 3, 1 },
                    { 4, 4, 1 },
                    { 5, 5, 1 },
                    { 6, 1, 2 },
                    { 7, 2, 2 },
                    { 8, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "ProductToShop",
                columns: new[] { "Id", "ProductId", "ShopId" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 4, 2, 1 },
                    { 2, 1, 2 },
                    { 3, 1, 3 },
                    { 5, 2, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductToGroups_GroupId",
                table: "ProductToGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductToGroups_ProductId",
                table: "ProductToGroups",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductToShop_ProductId",
                table: "ProductToShop",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductToShop_ShopId",
                table: "ProductToShop",
                column: "ShopId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductToGroups");

            migrationBuilder.DropTable(
                name: "ProductToShop");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Shops");
        }
    }
}
