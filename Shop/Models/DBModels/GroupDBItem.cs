﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.Models.DBModels
{
    public class GroupDBItem : BaseDBItem
    {
        public int? ParentId { get; set; }

    }
}
