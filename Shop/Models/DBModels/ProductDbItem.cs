﻿using Shop.Models.DBModels;
using Shop.Models.RESTModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.Models
{
    public class ProductDbItem : BaseDBItem
    {
        public double Price { get; set; }
        public double PriceVat { get; set; }
        public DateTime AddedTime { get; set; }
        public double VatRate { get; set; }
    }
}
