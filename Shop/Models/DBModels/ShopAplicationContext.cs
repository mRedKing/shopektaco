﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models.DBModels
{
    public class ShopAplicationContext : DbContext
    {

        public ShopAplicationContext(DbContextOptions<ShopAplicationContext> options) : base(options)
        {
            Database.Migrate();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductDbItem>().HasData(
                new List<ProductDbItem> {
                    new ProductDbItem
                    {
                        Id = 1,
                        Name = "Apple",
                        Price = 300,
                        PriceVat = 450,
                        VatRate = 0.5,
                        AddedTime = System.DateTime.Now,
                    },
                    new ProductDbItem
                    {
                        Id = 2,
                        Name = "Orange",
                        Price = 200,
                        PriceVat = 400,
                        VatRate = 2,
                        AddedTime = System.DateTime.Now,
                    }                }
            );

            modelBuilder.Entity<ShopDbItem>().HasData(
                new List<ShopDbItem> {
                    new ShopDbItem
                    {
                        Id = 1,
                        Name = "Shop#1",
                    },
                    new ShopDbItem
                    {
                        Id = 2,
                        Name = "Shop#2",
                    },
                    new ShopDbItem
                    {
                        Id = 3,
                        Name = "Shop#3",
                    }
                }
            );

            modelBuilder.Entity<GroupDBItem>().HasData(
                new List<GroupDBItem> {
                    new GroupDBItem
                    {
                        Id = 1,
                        Name = "Main",
                        ParentId = null
                    },
                    new GroupDBItem
                    {
                        Id = 2,
                        Name = "Sub1",
                        ParentId = 1
                    },
                    new GroupDBItem
                    {
                        Id = 3,
                        Name = "Subsub1",
                        ParentId = 2
                    },
                    new GroupDBItem
                    {
                        Id = 4,
                        Name = "Sub2",
                        ParentId = 1
                    },
                    new GroupDBItem
                    {
                        Id = 5,
                        Name = "SubSub2",
                        ParentId = 4
                    },new GroupDBItem
                    {
                        Id = 6,
                        Name = "Subsub3",
                        ParentId = 2
                    },new GroupDBItem
                    {
                        Id = 7,
                        Name = "Subsub5",
                        ParentId = 4
                    },
                }
            );

            modelBuilder.Entity<ProductToGroups>().HasData(
                new List<ProductToGroups> {
                    new ProductToGroups
                    {
                        Id = 1,
                        GroupId = 1,
                        ProductId = 1
                    },
                    new ProductToGroups
                    {
                        Id = 2,
                        GroupId = 2,
                        ProductId = 1
                    },
                    new ProductToGroups
                    {
                        Id = 3,
                        GroupId = 3,
                        ProductId = 1
                    },
                    new ProductToGroups
                    {
                        Id = 4,
                        GroupId = 4,
                        ProductId = 1
                    },
                    new ProductToGroups
                    {
                        Id = 5,
                        GroupId = 5,
                        ProductId = 1
                    },
                    new ProductToGroups
                    {
                        Id = 6,
                        GroupId = 1,
                        ProductId = 2
                    },
                    new ProductToGroups
                    {
                        Id = 7,
                        GroupId = 2,
                        ProductId = 2
                    },new ProductToGroups
                    {
                        Id = 8,
                        GroupId = 3,
                        ProductId = 2
                    },
                }
            );

            modelBuilder.Entity<ProductToShop>().HasData(
                new List<ProductToShop> {
                    new ProductToShop
                    {
                        Id = 1,
                        ShopId = 1,
                        ProductId = 1
                    },
                    new ProductToShop
                    {
                        Id = 2,
                        ShopId = 2,
                        ProductId = 1
                    },
                    new ProductToShop
                    {
                        Id = 3,
                        ShopId = 3,
                        ProductId = 1
                    },
                    new ProductToShop
                    {
                        Id = 4,
                        ShopId = 1,
                        ProductId = 2
                    },
                    new ProductToShop
                    {
                        Id = 5,
                        ShopId = 3,
                        ProductId = 2
                    },
                }
            );

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<GroupDBItem> Groups { get; set; }
        public DbSet<ProductDbItem> Products { get; set; }
        public DbSet<ShopDbItem> Shops { get; set; }
        public DbSet<ProductToShop> ProductToShop { get; set; }
        public DbSet<ProductToGroups> ProductToGroups { get; set; }
    }
}

