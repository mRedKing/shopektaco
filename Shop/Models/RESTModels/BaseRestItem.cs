﻿
namespace Shop.Models.RESTModels
{
    public class BaseRestItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
