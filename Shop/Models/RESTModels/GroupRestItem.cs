﻿using System.Collections.Generic;

namespace Shop.Models.RESTModels
{
    public class GroupRestItem : BaseRestItem
    {
        public int? ParentId { get; set; }
        public List<GroupRestItem> Child { get; set; }
    }
}
