﻿using System;
using System.Collections.Generic;

namespace Shop.Models.RESTModels
{
    public class ProductRestItem : BaseRestItem
    {
        public double? Price { get; set; }
        public double? PriceVat { get; set; }
        public DateTime AddedTime { get; set; }
        public double? VatRate { get; set; }
        public List<ShopRestItem> Shops { get; set; }
        public List<GroupRestItem> Groups { get; set; }

    }
}
