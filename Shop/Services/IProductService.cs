﻿using System.Collections.Generic;
using Shop.Models.RESTModels;


namespace Shop.Services
{
    public interface IProductService
    {
        List<ProductRestItem> GetProducts();

        ProductRestItem GetProduct(int id);

        ProductRestItem AddProduct(ProductRestItem product);

    }
}
