﻿using System.Collections.Generic;
using System.Linq;
using Shop.Models;
using Shop.Models.DBModels;
using Shop.Models.RESTModels;

namespace Shop.Services
{
    public class ProductService : IProductService
    {
        readonly ShopAplicationContext appContext;
        public ProductService(ShopAplicationContext context)
        {
            appContext = context;
        }

        public List<ProductRestItem> GetProducts()
        {
            List<ProductRestItem> productItems = appContext.Products.Select(p => ConvertProductToRest(p)).ToList();
            productItems.ForEach(p => p.Shops = appContext.ProductToShop.Where(e => e.ProductId == p.Id).Select(e => ConvertShopToRest(e.Shop)).ToList());
            productItems.ForEach(p => p.Groups = MergeGroupToThree(appContext.ProductToGroups.Where(e => e.ProductId == p.Id).Select(e => ConvertGroupToRest(e.Group)).ToList()));
            return productItems;
        }

        public ProductRestItem AddProduct(ProductRestItem product)
        {
            if (product.PriceVat != null && product.VatRate != null && product.Price == null)
            {
                return null;
            }
            else if (product.PriceVat == null && product.VatRate != null && product.Price != null)
            {
                product.PriceVat = product.Price * (1 + product.VatRate);
            }
            else if (product.PriceVat != null && product.VatRate == null && product.Price != null)
            {
                product.VatRate = product.PriceVat / product.Price;
            }
            else if (product.PriceVat != null && product.VatRate != null && product.Price == null)
            {
                product.Price = product.PriceVat / (product.VatRate + 1);
            }
            if (product.Shops != null && product.Shops.Count > 0)
            {
                product.Shops.ForEach(e => appContext.Shops.Add(ConvertShopToDb(e)));
                product.Shops.ForEach(e => appContext.ProductToShop.Add(new ProductToShop { ProductId = product.Id, ShopId = e.Id }));
            }
            if (product.Groups != null && product.Groups.Count > 0)
            {
                var tmp = TransformFromThree(product.Groups);
                tmp.ForEach(e => appContext.Groups.Add(ConvertGroupToDb(e)));
                tmp.ForEach(e => appContext.ProductToGroups.Add(new ProductToGroups { ProductId = product.Id, GroupId = e.Id }));

            }

            appContext.Products.Add(ConvertProductToDB(product));
            appContext.SaveChanges();
            return product;
        }

        public ProductRestItem GetProduct(int id)
        {

            if (appContext.Products.FirstOrDefault(p => p.Id == id) == null)
            {
                return null;
            }
            var productItem = ConvertProductToRest(appContext.Products.FirstOrDefault(p => p.Id == id));
            productItem.Shops = appContext.ProductToShop.Where(e => e.ProductId == productItem.Id).Select(e => ConvertShopToRest(e.Shop)).ToList();
            productItem.Groups = MergeGroupToThree(appContext.ProductToGroups.Where(e => e.ProductId == productItem.Id).Select(e => ConvertGroupToRest(e.Group)).ToList());
            return productItem;
        }

        private ProductRestItem ConvertProductToRest(ProductDbItem productDb)
        {
            return new ProductRestItem()
            {
                Id = productDb.Id,
                Name = productDb.Name,
                Price = productDb.Price,
                PriceVat = productDb.PriceVat,
                VatRate = productDb.VatRate,
                AddedTime = productDb.AddedTime,
            };
        }
        private ProductDbItem ConvertProductToDB(ProductRestItem productRest)
        {
            return new ProductDbItem()
            {
                Id = 0,
                Name = productRest.Name,
                Price = (double)productRest.Price,
                PriceVat = (double)productRest.PriceVat,
                VatRate = (double)productRest.VatRate,
                AddedTime = productRest.AddedTime,
            };

        }
        private ShopRestItem ConvertShopToRest(ShopDbItem productDb)
        {
            return new ShopRestItem()
            {
                Id = productDb.Id,
                Name = productDb.Name,
            };
        }
        private ShopDbItem ConvertShopToDb(ShopRestItem productDb)
        {
            return new ShopDbItem()
            {
                Id = 0,
                Name = productDb.Name,
            };
        }
        private GroupRestItem ConvertGroupToRest(GroupDBItem productDb)
        {
            return new GroupRestItem()
            {
                Id = productDb.Id,
                Name = productDb.Name,
                ParentId = productDb.ParentId
            };
        }
        private GroupDBItem ConvertGroupToDb(GroupRestItem productDb)
        {
            return new GroupDBItem()
            {
                Id = 0,
                Name = productDb.Name,
                ParentId = productDb.ParentId
            };
        }

        private List<GroupRestItem> MergeGroupToThree(List<GroupRestItem> groups, int? parentId = null)
        {
            var result = groups;

            if (groups.All(p => p.ParentId == parentId))
            {
                return groups;
            }
            var filteredGroups = groups.Where(p => p.ParentId == parentId).ToList();
            if (filteredGroups.Count() > 0)
            {
                result = filteredGroups;
                result.ForEach(p => p.Child = MergeGroupToThree(groups.Except(filteredGroups).ToList(), p.Id));
                return result;
            }
            else
            {
                return new List<GroupRestItem>();
            }
        }

        private List<GroupRestItem> TransformFromThree(List<GroupRestItem> groups)
        {
            var result = groups;

            if (groups.Count == 0 || groups.All(p => p.Child == null))
            {
                return groups;
            }

            result.Select(p => result.Union(TransformFromThree(p.Child)));
            return result;

        }
    }
}
